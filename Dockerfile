FROM juanper01/buster:2 AS build

ARG BAZEL_VERSION
ENV BAZEL_VERSION ${BAZEL_VERSION:-3.4.1}

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
       build-essential git libatomic1 openjdk-11-jdk-headless unzip wget zip \
    && update-java-alternatives --jre-headless -s java-1.11.0-openjdk-armhf \
    && apt-get clean \
    && rm -fr /var/lib/apt/lists/*

RUN git clone https://github.com/koenvervloesem/bazel-on-arm.git --branch master --single-branch /bazel-on-arm

WORKDIR /bazel-on-arm

RUN ./scripts/build_bazel.sh ${BAZEL_VERSION}

FROM scratch AS artifact
COPY --from=build /bazel-on-arm/bazel/output/bazel /bazel
